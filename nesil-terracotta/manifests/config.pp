class nesil-terracotta::config(
  $my_module_name       = "nesil-terracotta",
  $tc_home              = "/opt/terracotta",
  $tc_version           = "4.0.0",
  $tc_serverfilesdir	  = "/server_files",
  $tc_datadir           = "/terracotta_data",
  $tc_logsdir           = "/terracotta_logs",
  $tc_javahome			    = "/usr/lib/jvm/java-1.7.0-openjdk.x86_64",
  $tc_maxdatasize       = "5g",
  $tc_server1           = "",
  $tc_server2           = "",
  $tc_server_current    = "",
  $tc_server1_dns		    = undef,
  $tc_server2_dns		    = undef,
  $aws_dns_tag			    = "ec2.internal",
  $tc_user				      = "35004",
  $tc_group				      = "35004" 

){
  $require = Class["nesil-terracotta::install"]

  if $tc_server1_dns == undef {
      $server1_dns = regsubst($tc_server1,'\.', '-', 'G')
    }
  else
    {
      $server1_dns = $tc_server1_dns
    }

  if $tc_server2_dns == undef {
      $server2_dns = regsubst($tc_server2,'\.', '-', 'G')
    }
  else
    {
      $server2_dns = $tc_server2_dns
    }

    

    file { "${tc_home}${tc_serverfilesdir}":
         ensure => directory,
         owner  => "root",
         group  => "root",
         mode   => 0755
    } ->

    file { "${tc_home}${tc_serverfilesdir}${tc_datadir}":
         ensure => directory,
         owner  => "root",
         group  => "root",
         mode   => 0755
    } ->

    file { "${tc_home}${tc_serverfilesdir}${tc_logsdir}":
         ensure => directory,
         owner  => "root",
         group  => "root",
         mode   => 0755
    } ->
    
    ##terracotta license key
    file { "${tc_home}/bigmemory-max-${tc_version}/terracotta-license.key":
            ensure => present,
            source => "puppet:///modules/${my_module_name}/myterracotta-license.key",
            owner  => "root",
            group  => "root"
    } ->

    ##terracotta stop-tc-server.sh
    file { "${tc_home}/bigmemory-max-${tc_version}/server/bin/stop-tc-server.sh":
            ensure => present,
            content=> template("${my_module_name}/mystop-tc-server.sh.erb"),
            owner  => "${tc_user}",
            group  => "${tc_user}"
    } ->

    ##terracotta server-stat.sh
    file { "${tc_home}/bigmemory-max-${tc_version}/server/bin/server-stat.sh":
            ensure => present,
            content=> template("${my_module_name}/myserver-stat.sh.erb"),
            owner  => "${tc_user}",
            group  => "${tc_user}"
    } ->

    ##tc-config.xml 
    file { "${tc_home}/bigmemory-max-${tc_version}/server/bin/tc-config.xml":
          ensure => present,
          content=> template("${my_module_name}/mytc-config.xml.erb"),
          owner  => "${tc_user}",
          group  => "${tc_group}"
    } ->

    exec { "restart network":
      command     => "/etc/init.d/network restart",
      subscribe   => File["${tc_home}/bigmemory-max-${tc_version}/server/bin/tc-config.xml"],
      refreshonly => true
    } ->

    ##terracotta start-tc-server.sh
    file { "${tc_home}/bigmemory-max-${tc_version}/server/bin/start-tc-server.sh":
            ensure => present,
            content=> template("${my_module_name}/mystart-tc-server.sh.erb"),
            owner  => "${tc_user}",
            group  => "${tc_user}",
            notify  => File ['/etc/init.d/terracotta']
    } ->

    ##terracotta service sh
      file { "/etc/init.d/terracotta":
              ensure => present,
              content=> template("${my_module_name}/myterracotta.sh.erb"),
              owner  => "root",
              group  => "root",
              mode   => 0755,
              notify => Class [nesil-terracotta::service]
    }
}