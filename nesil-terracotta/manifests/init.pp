class nesil-terracotta(
  $my_module_name       = "nesil-terracotta",
  $tc_home              = "/opt/terracotta",
  $tc_version           = "4.0.0",
  $tc_update			      = true,
  $tc_downloadlink      = "http://d2zwv9pap9ylyd.cloudfront.net/bigmemory-max",
  $tc_serverfilesdir	  = "/server_files",
  $tc_datadir           = "/terracotta_data",
  $tc_logsdir           = "/terracotta_logs",
  $tc_javahome			    = "/usr/lib/jvm/java-1.7.0-openjdk.x86_64",
  $tc_maxdatasize       = "5g",
  $tc_server1           = "",
  $tc_server2           = "",
  $tc_server_current    = "",
  $tc_server1_dns		    = undef,
  $tc_server2_dns		    = undef,
  $aws_dns_tag			    = "ec2.internal",
  $tc_user				      = "35004",
  $tc_group				      = "35004" 

) {

	Exec { path => ["/usr/local/bin", "/usr/bin", "/usr/bin", "/bin", "/sbin"] }

	package { ["wget"]:
        ensure => installed
    }

  class { 'nesil-terracotta::install' :
        my_module_name       =>"${my_module_name}",
        tc_home              =>"${tc_home}",
        tc_version           =>"${tc_version}",
        tc_update            =>"${tc_update}",
        tc_downloadlink      =>"${tc_downloadlink}"
        }

  class { 'nesil-terracotta::config' :
        my_module_name       =>"${my_module_name}",
        tc_home              =>"${tc_home}",
        tc_version           =>"${tc_version}",
        tc_serverfilesdir    =>"${tc_serverfilesdir}",
        tc_datadir           =>"${tc_datadir}",
        tc_logsdir           =>"${tc_logsdir}",
        tc_javahome          =>"${tc_javahome}",
        tc_maxdatasize       =>"${tc_maxdatasize}",
        tc_server1           =>"${tc_server1}",
        tc_server2           =>"${tc_server2}",
        tc_server_current    =>"${tc_server_current}",
        tc_server1_dns       =>"${tc_server1_dns}",
        tc_server2_dns       =>"${tc_server2_dns}",
        aws_dns_tag          =>"${aws_dns_tag}",
        tc_user              =>"${tc_user}",
        tc_group             =>"${tc_group}"
  } 

  include nesil-terracotta::service 
   
    
}