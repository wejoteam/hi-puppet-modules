class nesil-terracotta::service{

	  service { "terracotta":
           enable     => true,
           ensure     => running,
           hasstatus  => false,
           hasrestart => true,
           require 	  => Class["nesil-terracotta::config"]
  }
}