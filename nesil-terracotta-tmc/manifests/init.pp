class nesil-terracotta-tmc(
  $my_module_name       = "nesil-terracotta-tmc",
  $tc_home              = "/opt/terracotta",
  $tc_version           = "4.0.0",
  $tc_update            = true,
  $tc_downloadlink      = "http://d2zwv9pap9ylyd.cloudfront.net/bigmemory-max",
  $tc_javahome          = "/usr/lib/jvm/java-1.7.0-openjdk.x86_64",
  $tc_user              = "35004",
  $tc_group             = "35004" 

) {

  Exec { path => ["/usr/local/bin", "/usr/bin", "/usr/bin", "/bin", "/sbin"] }

  package { ["wget"]:
        ensure => installed
    }

    class { 'nesil-terracotta-tmc::install' :
          my_module_name    =>"${my_module_name}",
          tc_home           =>"${tc_home}",
          tc_version        =>"${tc_version}",
          tc_update         =>"${tc_update}",
          tc_downloadlink   =>"${tc_downloadlink}"
    }

    class { 'nesil-terracotta-tmc::config' :
          my_module_name       =>"${my_module_name}",
          tc_home              =>"${tc_home}",
          tc_version           =>"${tc_version}",
          tc_downloadlink      =>"${tc_downloadlink}",
          tc_javahome          =>"${tc_javahome}",
          tc_user              =>"${tc_user}",
          tc_group             =>"${tc_group}"
    }

    include nesil-terracotta-tmc::service

    
}