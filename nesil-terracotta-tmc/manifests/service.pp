class nesil-terracotta-tmc::service{

	  service { "tmc":
           enable     => true,
           ensure     => running,
           hasstatus  => false,
           hasrestart => true,
           require 	  => Class["nesil-terracotta-tmc::config"]
  }
}