class nesil-terracotta-tmc::install(
  $my_module_name       = "nesil-terracotta-tmc",
  $tc_home              = "/opt/terracotta",
  $tc_version           = "4.0.0",
  $tc_update			      = true,
  $tc_downloadlink      = "http://d2zwv9pap9ylyd.cloudfront.net/bigmemory-max"

) {
  
    if $tc_update {

    file { "${tc_home}":
         ensure => directory,
         owner  => "root",
         group  => "root",
         mode   => 0755
    } ->

    wget::fetch { "terracotta_download":
      source      => "${tc_downloadlink}-${tc_version}.tar.gz",
      destination => "/usr/local/src/bigmemory-max-${tc_version}.tar.gz",
      } ->
  
    exec { "tmc_untar":
      command => "tar xf /usr/local/src/bigmemory-max-${tc_version}.tar.gz",
      cwd     => $tc_home,
      creates => "${tc_home}/bigmemory-max-${tc_version}",
      notify  => Class["nesil-terracotta-tmc::config"],
      } 
  }
}