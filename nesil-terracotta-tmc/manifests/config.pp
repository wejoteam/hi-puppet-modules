class nesil-terracotta-tmc::config(
  $my_module_name       = "nesil-terracotta-tmc",
  $tc_home              = "/opt/terracotta",
  $tc_version           = "4.0.0",
  $tc_downloadlink      = "http://d2zwv9pap9ylyd.cloudfront.net/bigmemory-max",
  $tc_javahome          = "/usr/lib/jvm/java-1.7.0-openjdk.x86_64",
  $tc_user              = "35004",
  $tc_group             = "35004" 

){
  $require = Class["nesil-terracotta-tmc::install"]

    ##terracotta license key
    file { "${tc_home}/bigmemory-max-${tc_version}/terracotta-license.key":
            ensure => present,
            source => "puppet:///modules/${my_module_name}/myterracotta-license.key",
            owner  => "root",
            group  => "root"
    } ->

    ##tmc stop-tmc.sh
    file { "${tc_home}/bigmemory-max-${tc_version}/tools/management-console/bin/stop-tmc.sh":
            ensure => present,
            content=> template("${my_module_name}/mystop-tmc.sh.erb"),
            owner  => "${tc_user}",
            group  => "${tc_user}"
    } ->

   
    ##tmc start-tmc.sh
    file { "${tc_home}/bigmemory-max-${tc_version}/tools/management-console/bin/start-tmc.sh":
            ensure => present,
            content=> template("${my_module_name}/mystart-tmc.sh.erb"),
            owner  => "${tc_user}",
            group  => "${tc_user}",
            notify => File[ '/etc/init.d/tmc' ]
    } ->

    ##terracotta-tmc service sh
    file { "/etc/init.d/tmc":
            ensure => present,
            content=> template("${my_module_name}/myterracotta-tmc.sh.erb"),
            owner  => "root",
            group  => "root",
            mode   => 0755,
            notify  => Class ['nesil-terracotta-tmc::service']
    } 
}