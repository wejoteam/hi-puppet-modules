class nesil-jetty::config(
  $my_module_name       = "nesil-jetty",
  $app_environment      = "",
  $home                 = "/opt",
  $jetty_version        = "9.2.3.v20140905",
  $jetty_homedir        = "/opt/jetty-server",
  $warfile_location     = "/opt/warfile",
  $warfile_name         = "",
  $application_xml      = "",
  $jenkins_accountname  = "",
  $jenkins_buildname    = "",
  $jenkins_buildnumber  = "lastSuccessfulBuild",
  $jenkins_username     = "",
  $jenkins_password     = ""
) {

  $require = Class["nesil-jetty::install"]
  
  ##myjetty.sh copy
  file { "${jetty_homedir }/bin/jetty.sh":
          ensure => present,
          content=> template("${my_module_name}/myjetty.sh.erb"),
          owner  => "root",
          group  => "root",
          mode   => 0755,
          notify  => Class["nesil-jetty::service"]
  } ->

  ##mystart.ini copy
  file { "${jetty_homedir }/start.ini":
          ensure => present,
          content=> template("${my_module_name}/mystart_${jetty_version}.ini.erb"),
          owner  => "root",
          group  => "root",
          mode   => 0664,
          notify  => Class["nesil-jetty::service"]
  } ->

  ##my_application.xml copy
  file { "${jetty_homedir }/webapps/${application_xml}":
          ensure => present,
          content=> template("${my_module_name}/myapplication.xml.erb"),
          owner  => "root",
          group  => "root",
          mode   => 0646,
          notify  => Class["nesil-jetty::service"]
  } ->

  ##jetty-requestlog.xml copy
  file { "${jetty_homedir }/etc/jetty-requestlog.xml":
          ensure => present,
          source => "puppet:///modules/${my_module_name}/requestlog.xml",
          owner  => "root",
          group  => "root",
          mode   => 0664,
          notify  => Class["nesil-jetty::service"]
  } ->

  ##jetty-logging.xml copy
  file { "${jetty_homedir }/etc/jetty-logging.xml":
          ensure => present,
          source => "puppet:///modules/${my_module_name}/logging.xml",
          owner  => "root",
          group  => "root",
          mode   => 0664,
          notify  => Class["nesil-jetty::service"]
  } ->

  file { "/opt/resources":
          ensure => directory,
          owner  => "root",
          group  => "root",
          mode   => 0755
  } ->

  ##application.properties copy
  file { "/opt/resources/application.properties":
          ensure => present,
          source => "puppet:///modules/${my_module_name}/application.properties",
          owner  => "root",
          group  => "root"
  } ->
  
  file { "/etc/init.d/jetty":
          ensure => present,
          content=> template("${my_module_name}/myjetty.sh.erb"),
          owner  => "root",
          group  => "root",
          mode   => 0755
  } ->
  #/opt/tempwarfile directory
  file { "/opt/tempwarfile":
        ensure => directory,
        owner  => "root",
        group  => "root",
        mode   => 0755
  } 
  if $deployable == 'true' {

  exec { "change deployable tag value to inProgress":
        environment => ["AWS_EC2_HOME=/opt/aws/apitools/ec2"],
        command => "/opt/aws/bin/ec2-create-tags --aws-access-key ${aws_access_key} --aws-secret-key ${aws_secret_key} --region ${aws_region} ${ec2_instance_id} --tag=\"deployable=inProgress\"",
        timeout => 0,
        require => File['/opt/tempwarfile']
  } ->

  exec { "remove instance from loadbalancer":
        environment => ["AWS_ELB_HOME=/opt/aws/apitools/elb"],
        command => "/opt/aws/bin/elb-deregister-instances-from-lb ${aws_loadbalancer_name} --instances ${ec2_instance_id} --region ${aws_region} --access-key-id ${aws_access_key} --secret-key ${aws_secret_key} ",
        timeout => 0
  } ->


  file { "remove old war file":
        ensure  => absent,
        path    => "/opt/tempwarfile/${warfile_name}",
        recurse => true,
        purge   => true,
        force   => true
  } ->


  file { "${$warfile_location}":
         ensure => directory,
         owner  => "root",
         group  => "root",
         mode   => 0755
  } ->

  exec { "warfile download from jenkins":
        cwd     => "/opt/tempwarfile",
        creates => "/opt/tempwarfile/${warfile_name}",
        command => "wget --auth-no-challenge --http-user=${jenkins_username} --http-password=${jenkins_password} https://${jenkins_accountname}.ci.cloudbees.com/job/${jenkins_buildname}/${jenkins_buildnumber}/artifact/api/build/libs/${warfile_name} ",
        timeout => 0,
        require => Package['wget']
  } ->

  exec { "add instance to loadbalancer":
        environment => ["AWS_ELB_HOME=/opt/aws/apitools/elb"],
        command => "/opt/aws/bin/elb-register-instances-with-lb ${aws_loadbalancer_name} --instances ${ec2_instance_id} --region ${aws_region} --access-key-id ${aws_access_key} --secret-key ${aws_secret_key} ",
        timeout => 0,
  } ->

  exec { "change deployable tag value to false":
        environment => ["AWS_EC2_HOME=/opt/aws/apitools/ec2"],
        command => "/opt/aws/bin/ec2-create-tags --aws-access-key ${aws_access_key} --aws-secret-key ${aws_secret_key} --region ${aws_region} ${ec2_instance_id} --tag=\"deployable=false\"",
        timeout => 0
  } ->

  file { "${$warfile_location}/${warfile_name}":
        ensure  => present,
        source  => "/opt/tempwarfile/${warfile_name}",
        replace => true,
        owner   => "root",
        group   => "root",
        notify  => Class["nesil-jetty::service"]
  }
  }
}