class nesil-jetty::service{

	  service { "jetty":
           enable     => true,
           ensure     => running,
           hasstatus  => false,
           hasrestart => true,
           require 	  => Class["nesil-jetty::config"]
  }
}