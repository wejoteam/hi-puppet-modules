class nesil-jetty(
  $my_module_name       = "nesil-jetty",
  $app_environment       = "",
  $home                 = "/opt",
  $jetty_version        = "9.2.3.v20140905",
  $jetty_homedir        = "/opt/jetty-server",
  $jetty_update         = true,
  $warfile_location     = "/opt/warfile",
  $warfile_name         = "",
  $application_xml      = "",
  $jenkins_accountname  = "",
  $jenkins_buildname    = "",
  $jenkins_buildnumber  = "lastSuccessfulBuild",
  $jenkins_username     = "",
  $jenkins_password     = ""
) {

Exec { path => ["/usr/local/bin", "/usr/bin", "/usr/bin", "/bin", "/sbin"] }
package { ["wget"]:
        ensure => installed
    }

class { nesil-jetty::install :
        home                 =>"${home}",
        jetty_version        =>"${jetty_version}",
        jetty_homedir        =>"${jetty_homedir}",
        jetty_update         =>"${jetty_update}" 
}

class { nesil-jetty::config:
        my_module_name       =>"${my_module_name}",
        app_environment      =>"${app_environment}",
        home                 =>"${home}",
        jetty_version        =>"${jetty_version}",
        jetty_homedir        =>"${jetty_homedir}",
        warfile_location     =>"${warfile_location}",
        warfile_name         =>"${warfile_name}",
        application_xml      =>"${application_xml}",
        jenkins_accountname  =>"${jenkins_accountname}",
        jenkins_buildname    =>"${jenkins_buildname}",
        jenkins_buildnumber  =>"${jenkins_buildnumber}",
        jenkins_username     =>"${jenkins_username}",
        jenkins_password     =>"${jenkins_password}"
  }

include nesil-jetty::service

}
