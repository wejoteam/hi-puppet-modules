class nesil-fe(
  $my_module_name   = "nesil-fe",
  $myenvironment		= "dev",
  $rds_dbname			  = "",
  $rds_username			= "",
  $rds_userpassword	= "",
  $rds_endpoint			= "",
  $myapiurl			    = "",
  $redis_host			  = ""

) {

Exec { path => ["/usr/local/bin", "/usr/bin", "/usr/bin", "/bin", "/sbin"] }

  ##public_html/development/shared/database.rb
  file { "/home/hi360/public_html/development/shared/database.rb":
          ensure => present,
          content=> template("${my_module_name}/mydatabase.rb.erb"),
          owner  => "hi360",
          group  => "hi360",
          mode   => 0644
  } ->

  ##public_html/development/current/config/database.rb
  file { "/home/hi360/public_html/development/current/config/database.rb":
          ensure => present,
          content=> template("${my_module_name}/mydatabase.rb.erb"),
          owner  => "hi360",
          group  => "hi360",
          mode   => 0644
  } ->

  ##public_html/development/current/config/apps.rb
  file { "/home/hi360/public_html/development/current/config/apps.rb":
          ensure => present,
          content=> template("${my_module_name}/myapps_${myenvironment}.rb.erb"),
          owner  => "hi360",
          group  => "hi360",
          mode   => 0644
  } 

}